import Dependencies._

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "com.github.amitsk",
      scalaVersion := "2.12.3",
      version      := "0.1.0-SNAPSHOT"
    )),
    name := "Learning Cats",
    libraryDependencies += cats,
    libraryDependencies += scalaTest % Test
  )

scalacOptions += "-Ypartial-unification"
scalacOptions += "-language:higherKinds"
