package com.github.amitsk.freemonad

object FreeMonads {

  trait Monoid[A] {
    def append(a: A, b: A): A
    def identity: A
  }

  class ListConcat[A] extends Monoid[List[A]] {
    def append(a: List[A], b: List[A]): List[A] = a ++ b
    def identity: List[A] = List.empty[A]
  }

  sealed trait Free[F[_], A] { self =>
    def flatMap[B] (fn: A => Free[F, B]): Free[F,B] =
      FlatMap(self, (a: A) =>fn(a) )
  }

  case class Return[F[_], A](given: A) extends Free[F, A]

  case class Suspend[F[_], A](fn: F[A]) extends Free[F, A]

  case class FlatMap[F[_], A, B](free: Free[F, A], fn: A => Free[F, B]) extends Free[F, B]


}
